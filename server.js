const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const ArtistRouter = require('./app/ArtistRouter');
const AlbumRouter = require('./app/AlbumRouter');
const TrackRouter = require('./app/TrackRouter');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(()=>{
  app.use('/artists', ArtistRouter());
  app.use('/albums', AlbumRouter());
  app.use('/tracks', TrackRouter());
  app.listen(port, ()=>{
    console.log(`We are use ${port}`);
  })
})