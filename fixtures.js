const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./modules/ArtistModule');
const Album = require('./modules/AlbumModule');
const Track = require('./modules/TrackModule');

const run = async () => {


  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [Ilkay, timati] = await Artist.create(
    {
      name: 'Ilkay Sencan',
      photo: 'il.jpg',
      information: "\n" +
        "Ilkay Sencan - Turkish musician from the city of Istanbul, working" +
        " in the style of electronic dance music (more precisely, Nu Disco, Deep House, Bass" +
        " House, Future Bass), works with the following recording studios: Ultra Music, Enormous " +
        "Tunes, UP Club Records from 2014 to the present time. The most popular songs are Ilkay Sencan:" +
        " \"Do it\", \"What You Want\", \"Let me\", \"No sweet\", \"Back to life\"."
    },
    {
      name: 'Timati',
      photo: 'timat.jpg',
      information: "\n" +
        "Timur Il'darovich Yunusov, better known as T'mati - a" +
        " hip-hop performer, music producer, actor and entrepreneur, is also a graduate" +
        " of \"Star Factory 4\". Member of the" +
        " Board of Trustees of the Moscow International Fund with the assistance of UNESCO."
    }
  );

  const [Lol, amg] = await Album.create(
    {
      name: "Trata",
      artist: Ilkay._id,
      releas: 2012
    },
    {
      name: "amg",
      artist: timati._id,
      releas: 2015,
      image: 'blac.jpeg'
    },
  );

  await Track.create(
    {
      name: "Kek",
      album: Lol._id,
      duration: "4:12"
    },
    {
      name: "amg",
      album: amg._id,
      duration: "3:56"
    },
    {
      name: "lada",
      album: amg._id,
      duration: "3:25"
    },
    {
      name: "Trol'",
      album: Lol._id,
      duration: "3:45"
    }
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something went wrong!', error);
});