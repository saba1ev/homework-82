const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  artist:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  releas: {
    type: Number
  },
  image: {
    type: String
  }
});

const AlbumModule = mongoose.model('Album', AlbumSchema);
module.exports = AlbumModule;