const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  photo: {
    type: String
  },
  information: {
    type: String
  }
});

const ArtistModule = mongoose.model('Artist', ArtistSchema);
module.exports = ArtistModule;